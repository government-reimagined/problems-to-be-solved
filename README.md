# How to use this repository
The idea here is that every aspect of our society is broken. We will record it in the issues list here, and plans will be made to deal with these issues and put them into the project. **It may take some time to get round to your issue. BE PATIENT**

## Submitting an issue:
mmm I haven't worked out the best way to do this yet without provoking spam. Any ideas?

## Once an issue has been received it will be given a label (category) and a proposed deal with date. 
Because this is a new society that we are building, we are not proposing to solve real-world problems. What we intend to do is list the issues and try to avoid problems when building the rules for the new world.